  package oficial;
import robocode.*;
import java.awt.Color;

public class Centaurum extends Robot
{

	boolean xy = false;
	boolean melee = false;
	int dir =1;
	public void run() {

		Color b = new Color(0,0,0);
		Color g = new Color(134,2,41);	
		Color r = new Color(202,2,62);	
		Color s = new Color(255,0,0);	
		Color a = new Color(255,255,255);
		
		setColors(b,  g,  r,  s,  a);
		
		if(getX() <= getY() &&  getX() <= getBattleFieldHeight()-getY() && getX() <= getBattleFieldWidth()-getX()){
			if(getHeading()<360){
				turnLeft(getHeading());
			}else{
				turnRight(180-getHeading());
			}	
		}else if(getY() <= getBattleFieldHeight()-getY() && getY() <= getBattleFieldWidth()-getX()){
			if(getHeading()<270){
				turnLeft(getHeading());
			}else{
				turnRight(90-getHeading());
			}
		}else if(getBattleFieldHeight()-getY() <= getBattleFieldWidth()-getX()){
			if(getHeading()<90){
				turnLeft(getHeading());
			}else{
				turnRight(270-getHeading());
			}
		}else{
			if(getHeading()<180){
				turnLeft(getHeading());
			}else{
				turnRight(360-getHeading());
			}
		}
	
		
		ahead(getBattleFieldHeight());
		turnGunLeft(90);
		int moves =2;
		
		while(true) {
			if(melee){
				turnRight(5 * dir);
			}else{
				if(xy){
					moves = 3;
				}
				for(int i = 0;i<moves;i++){
					ahead(200);
					xy = (!xy);
					moves=2;	
				}
			}	
		}
	}

	
	public void onScannedRobot(ScannedRobotEvent e) {
		fireBullet(3);
		if(melee){
			if (e.getBearing() >= 0) {
				dir = 1;
			} else {
				dir = -1;
			}

			turnRight(e.getBearing());
			
			if(e.getDistance()>getBattleFieldHeight()/2){
				melee =false;
				
		if(getX() <= getY() &&  getX() <= getBattleFieldHeight()-getY() && getX() <= getBattleFieldWidth()-getX()){
			if(getHeading()<360){
				turnLeft(getHeading());
			}else{
				turnRight(180-getHeading());
			}	
		}else if(getY() <= getBattleFieldHeight()-getY() && getY() <= getBattleFieldWidth()-getX()){
			if(getHeading()<270){
				turnLeft(getHeading());
			}else{
				turnRight(90-getHeading());
			}
		}else if(getBattleFieldHeight()-getY() <= getBattleFieldWidth()-getX()){
			if(getHeading()<90){
				turnLeft(getHeading());
			}else{
				turnRight(270-getHeading());
			}
		}else{
			if(getHeading()<180){
				turnLeft(getHeading());
			}else{
				turnRight(360-getHeading());
			}
		}
	
		ahead(getBattleFieldHeight());
		turnGunLeft(90);
				
	}else{
		ahead(e.getDistance() + 5);			
		}	
			scan();
		}
	}
	
	public void onWin(WinEvent e){
		turnLeft(30);
		turnLeft(-30);
		onWin(e);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		 if(melee == false){ 
	 		melee = true;
			turnGunLeft(-90);
		}	
	}

	public void onHitRobot(HitRobotEvent e){
		if(melee == false){
			melee = true;
			turnGunLeft(-90);
			if(e.getBearing()>=getHeading()+180 ){
				turnRight((360-e.getBearing())+getHeading());
			}else if(e.getBearing()<getHeading() && e.getBearing()>=getHeading()-180){
				turnRight(getHeading()-e.getBearing());
			}else{
				turnRight(-e.getBearing());
			}
		}
	}

	public void onHitWall(HitWallEvent e) {	
		turnLeft(90);
		if(melee){
			melee = false;
			turnGunLeft(90);
		}
	}
	
}
